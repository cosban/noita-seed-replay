# Noita Seed Replay

> Replay the current world again, easily

## Installation

1. Download & Copy files to the mod folder (There should be two folders copied)
2. Open Noita & Enable the seed_redo mod
3. Enjoy

## Usage

From the point the mod is enabled, you will have the option to replay the world that you are currently in via a newly added game mode.
To replay the world, choose the new game mode when selecting new game.
