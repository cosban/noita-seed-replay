local previousSeed = ModSettingGet("recent_seed")
if (ModIsEnabled("redo_seed_mode")) then
	SetWorldSeed(previousSeed)
end

function OnMagicNumbersAndWorldSeedInitialized()
	local seed = StatsGetValue("world_seed")
	ModSettingSet("recent_seed", seed)
end

-- -- This code runs when all mods' filesystems are registered
-- ModMagicNumbersFileAdd( "mods/redo_seed/files/magic_numbers.xml" ) -- Will override some magic numbers using the specified file